package com.asi2.pcbm.common.exceptions;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class UserError extends Exception {
    public UserError(String code){
        super(String.format("errors.user.%s", code));
    }

}



