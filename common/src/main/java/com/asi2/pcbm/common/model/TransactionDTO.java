package com.asi2.pcbm.common.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDTO implements Serializable {
	private Integer id;
    @DecimalMin("0.0")
    private BigDecimal price;
    private Integer cardId;
    private Integer  buyerId;
    private Integer  sellerId;
    private Date transactionDate;
}
