package com.asi2.pcbm.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroAuth {

	public static void main(String[] args) {
		SpringApplication.run(MicroAuth.class, args);
	}

}
