package com.asi2.pcbm.auth.controller;

import com.asi2.pcbm.common.exceptions.AuthError;
import com.asi2.pcbm.common.model.LoginDTO;
import com.asi2.pcbm.common.model.UserDTO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@RestController
public class AuthController {
	private final UserService userService;

	public AuthController(UserService uService) {
		this.userService = uService;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/api/auth")
	public LoginDTO login(@RequestBody LoginDTO loginForm, HttpServletResponse response) {
		UserDTO user = userService.checkLogin(loginForm);
		if (user == null){
			throw new AuthError("bad_login");
		}
		Cookie cookie = new Cookie("login", user.getLogin());
		cookie.setPath("/");
		response.addCookie(cookie);
		loginForm.setPassword(null);
		return loginForm;
	}

}
