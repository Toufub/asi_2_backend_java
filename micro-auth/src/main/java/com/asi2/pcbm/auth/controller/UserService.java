package com.asi2.pcbm.auth.controller;

import com.asi2.pcbm.common.model.LoginDTO;
import com.asi2.pcbm.common.model.UserDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class UserService {
    private final RestTemplate restTemplate;

    private final String UserEndpoint;

    public UserService(@Value("${user.endpoint:http://localhost:8083/api/}") String userEndpoint, RestTemplateBuilder restTemplateBuilder){
        this.UserEndpoint = userEndpoint;
        this.restTemplate = restTemplateBuilder.rootUri(UserEndpoint).build();
    }

    public UserDTO checkLogin(LoginDTO loginForm) throws ResponseStatusException {
        try {
            Optional<UserDTO> userOptional = Optional.ofNullable(findByUsername(loginForm.getUsername()));
            if(userOptional.isPresent() && userOptional.get().getPwd() != null){
                String password = loginForm.getPassword();
                if(password.equals(userOptional.get().getPwd())){
                    return userOptional.get();
                }
            }
        } catch (HttpClientErrorException  e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getStatusText(), e);
        }
        return null;
    }

    public UserDTO findByUsername(String username){
        return this.restTemplate.getForObject(UserEndpoint + "/user/" + username, UserDTO.class);
    }
}
