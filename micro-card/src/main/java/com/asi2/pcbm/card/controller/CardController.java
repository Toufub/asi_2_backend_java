package com.asi2.pcbm.card.controller;

import com.asi2.pcbm.common.exceptions.CardError;
import com.asi2.pcbm.card.model.Card;
import com.asi2.pcbm.common.model.CardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class CardController {
	@Autowired
	private final CardService cService;

	public CardController(CardService cService) {
		this.cService = cService;
	}

	@RequestMapping(value = "/api/card", method = RequestMethod.POST)
	public Card addCard(@RequestBody CardDTO cardDto) {
		return cService.addCard(cardDto);
	}

	@RequestMapping(value = "/api/card/{id}", method = RequestMethod.GET)
	public Card getCard(@PathVariable int id) {
		try {
			return cService.getCard(id);
		} catch (CardError e){
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/api/cards_to_sell", method = RequestMethod.GET)
	public Iterable<Card> getCardToSell(){
		try {
			return cService.getCardToSell();
		} catch (Exception e){
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		}
	}
	@RequestMapping(value = "/api/cards_to_sell/{userId}", method = RequestMethod.GET)
	public Iterable<Card> getUserCardToSell(@PathVariable int userId){
		try {
			return cService.getUserCardToSell(userId);
		} catch (Exception e){
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/api/cards")
	public Iterable<Card> getCards() {
		return cService.getCards();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/api/cardsUser/{id}")
	public Iterable<Card> getCardsUser(@PathVariable int id) {
		return cService.getCardsUser(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value="/api/card/{id}")
	public Card modifyCard(@RequestBody CardDTO newCard, @PathVariable Integer id) {
		try {
			return this.cService.modifyCard(id, newCard);
		} catch (CardError e){
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		}
	}
	@RequestMapping(method = RequestMethod.PUT, value="/api/card_userid/{id}")
	public Card modifyCardUserId(@RequestBody CardDTO newCard, @PathVariable Integer id) {
		try {
			return this.cService.modifyCard(id, newCard.getUserId());
		} catch (CardError e){
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		}
	}
	@RequestMapping(method = RequestMethod.PUT, value="/api/card_price/{id}")
	public Card modifyCardPrice(@RequestBody CardDTO newCard, @PathVariable Integer id) {
		try {
			return this.cService.modifyCard(id, newCard.getPrice());
		} catch (CardError e){
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		}
	}

	@RequestMapping(method = RequestMethod.DELETE, value="/api/card/{id}")
	public void deleteCard(@PathVariable Integer id) {
		try {
			this.cService.deleteCard(id);
		} catch (CardError e){
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		}
	}

}
