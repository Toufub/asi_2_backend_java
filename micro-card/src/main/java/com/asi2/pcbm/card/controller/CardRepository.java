package com.asi2.pcbm.card.controller;

import com.asi2.pcbm.card.model.Card;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CardRepository extends CrudRepository<Card, Integer> {
    @Query("SELECT c FROM Card c WHERE c.userId = ?1")
    public List<Card> findByOwner(int userId);

    public List<Card> findByName(String name);

    @Query("SELECT c FROM Card c WHERE c.userId > 0 AND c.price > 0")
    public List<Card> findCardsToSell();
    @Query("SELECT c FROM Card c WHERE c.userId = ?1 AND c.price > 0")
    public List<Card> findUserCardsToSell(int userId);
}
