package com.asi2.pcbm.card.controller;

import java.util.List;

import com.asi2.pcbm.common.exceptions.CardError;
import com.asi2.pcbm.card.tools.CardMapper;
import com.asi2.pcbm.common.model.CardDTO;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.asi2.pcbm.card.model.Card;

@Service
public class CardService {
	private final Faker faker = new Faker();
	@Autowired
	private final CardRepository cRepo;
	
	public CardService(CardRepository cRepo) {
		this.cRepo=cRepo;
	}

	public Card addCard(CardDTO c) {
		return cRepo.save(CardMapper.FromDTOToCard(c));
	}

	public CardDTO generateCard(int userId){
		CardDTO card = new CardDTO();
		card.setName(faker.name().name());
		card.setDescription(faker.chuckNorris().fact());
		card.setAffinity(faker.beer().style());
		card.setDefence(Math.round(faker.random().nextDouble()*10));
		card.setAttack(Math.round(faker.random().nextDouble()*10));
		card.setFamily(faker.book().genre());
		card.setHp(Math.round(faker.random().nextDouble()*10));
		card.setEnergy(faker.random().nextInt(1, 10));
		card.setUserId(userId);
		card.setStoreId(1);
		card.setImgUrl("https://picsum.photos/500");
		card.setSmallImgUrl("https://picsum.photos/500");
		return card;
	}

	public Card modifyCard(Integer id, CardDTO newCard) {
		return this.cRepo.findById(id)
				.map(card -> {
					newCard.setId(id);
					return cRepo.save(CardMapper.FromDTOToCard(newCard));
				})
				.orElseThrow(() -> new CardError("not_found"));
	}

	public Card modifyCard(Integer id, Card newCard) {
		getCard(id);
		return cRepo.findById(id)
				.map(card -> {
					newCard.setId(id);
					return cRepo.save(newCard);
				})
				.orElseThrow(() -> new CardError("not_found"));
	}
	public Card modifyCard(Integer id, Double newPrice) {
		return this.cRepo.findById(id)
				.map(card -> {
					card.setPrice(newPrice);
					return this.cRepo.save(card);
				})
				.orElseThrow(() -> new CardError("not_found"));
	}
	public Card modifyCard(Integer id, Integer userId) {
		return this.cRepo.findById(id)
				.map(card -> {
					card.setUserId(userId);
					return cRepo.save(card);
				})
				.orElseThrow(() -> new CardError("not_found"));
	}

	public Card getCard(int id) throws CardError {
		return cRepo.findById(id)
				.orElseThrow(()-> new CardError("not_found"));
	}

	public Iterable<Card> getCards() {
		return cRepo.findAll();
	}

	public List<Card> getCardsUser(int id) {
		return cRepo.findByOwner(id);
	}

	public void deleteCard(int id) throws CardError {
		this.getCard(id);
		cRepo.deleteById(id);
	}

	public List<Card> getCardToSell(){
		return cRepo.findCardsToSell();
	}
	public List<Card> getUserCardToSell(int userId){
		return cRepo.findUserCardsToSell(userId);
	}




}
