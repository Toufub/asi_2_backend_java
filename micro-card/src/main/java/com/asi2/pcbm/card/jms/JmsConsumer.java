package com.asi2.pcbm.card.jms;

import com.asi2.pcbm.card.config.JmsConfig;
import com.asi2.pcbm.card.controller.CardService;
import com.asi2.pcbm.common.model.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JmsConsumer {


    @Autowired
    private final CardService cardService;

    @Autowired
    JmsConfig jmsConfig;

    public JmsConsumer(CardService cardService) {
        this.cardService = cardService;
    }

    @JmsListener(destination = "card.add_to_new_user")
    public void onCreateMessage(UserDTO user) {
        try{
            log.info("Received Message: "+ user.getLogin());
            int i = 0;
            while (i < 5){
                cardService.addCard(cardService.generateCard(user.getId()));
                i++;
            }
            // TODO : Notif
        } catch(Exception e) {
            log.error("Received Exception : "+ e);
        }
    }

}
