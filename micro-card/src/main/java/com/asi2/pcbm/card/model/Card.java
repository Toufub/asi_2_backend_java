package com.asi2.pcbm.card.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "cards")
@Getter
@Setter
@NoArgsConstructor
public class Card {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "card_gen")
	@SequenceGenerator(name= "card_gen", sequenceName = "card_seq", initialValue = 6)
	private int id;
	private String name;
	private String description;
	private String family;
	private String affinity;
	private String imgUrl;
	private String smallImgUrl;
	private Integer energy;
	private Long hp;
	private Long defence;
	private Long attack;
	private Double price;
	private int userId;
	private int storeId;
}
