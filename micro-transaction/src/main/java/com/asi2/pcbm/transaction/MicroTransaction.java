package com.asi2.pcbm.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroTransaction {

	public static void main(String[] args) {
		SpringApplication.run(MicroTransaction.class, args);
	}

}
