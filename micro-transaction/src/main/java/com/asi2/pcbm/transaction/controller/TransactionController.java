package com.asi2.pcbm.transaction.controller;

import java.util.List;

import com.asi2.pcbm.common.entities.BuyOrderEntity;
import com.asi2.pcbm.common.entities.SellOrderEntity;
import com.asi2.pcbm.transaction.jms.JmsProducer;
import org.springframework.web.bind.annotation.*;
import com.asi2.pcbm.common.model.TransactionDTO;

@RestController
public class TransactionController {
	private final TransactionService transactionService;
	private JmsProducer jmsProducer;

	public TransactionController(TransactionService transactionService, JmsProducer jmsProducer) {
		this.transactionService = transactionService;
		this.jmsProducer = jmsProducer;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/api/transactions")
	public List<TransactionDTO> getTransactionList() {
		return this.transactionService.getAllTransactions();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/api/transaction/buy")
	public BuyOrderEntity sendBuyingOrder(@RequestBody BuyOrderEntity buyOrder) {
		this.jmsProducer.update(buyOrder);
		return buyOrder;
	}
	@RequestMapping(method = RequestMethod.POST, value = "/api/transaction/sell")
	public SellOrderEntity sendSellingOrder(@RequestBody SellOrderEntity sellOrder) {
		this.jmsProducer.update(sellOrder);
		return sellOrder;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/api/transaction/{id}")
	public TransactionDTO getTransaction(@PathVariable Integer id) {
		return this.transactionService.getTransaction(id);
	}
}
