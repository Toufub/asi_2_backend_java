package com.asi2.pcbm.transaction.controller;

import com.asi2.pcbm.transaction.model.Transaction;
import org.springframework.data.repository.CrudRepository;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

}
