package com.asi2.pcbm.transaction.jms;

import com.asi2.pcbm.common.entities.BuyOrderEntity;
import com.asi2.pcbm.common.entities.SellOrderEntity;
import com.asi2.pcbm.common.model.CardDTO;
import com.asi2.pcbm.common.model.TransactionDTO;
import com.asi2.pcbm.common.model.UserDTO;
import com.asi2.pcbm.transaction.config.JmsConfig;
import com.asi2.pcbm.transaction.controller.TransactionService;
import com.asi2.pcbm.common.exceptions.TransactionError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class JmsConsumer {

    @Value("${user.endpoint}")
    private String UserEndpoint;
    @Value("${card.endpoint}")
    private String CardEndpoint;

    @Autowired
    private final TransactionService transactionService;

    private final RestTemplate restTemplate;

    @Autowired
    JmsConfig jmsConfig;

    public JmsConsumer(TransactionService transactionService,
                       RestTemplateBuilder restTemplateBuilder) {
        this.transactionService = transactionService;
        this.restTemplate = restTemplateBuilder.build();
    }

    @JmsListener(destination = "transaction.sell")
    @SendTo("notif.send")
    public int onSellOrder(SellOrderEntity sellOrder){
        try {
            log.info("Retrieved sellOrder : "+sellOrder);
            if (!sellOrder.getAction().equals("SELL")){
                throw new TransactionError("bad_request");
            }

            CardDTO card = getCard(sellOrder.getCard_id());
            if (card.getUserId() != sellOrder.getUser_id()){
                throw new TransactionError("not_yours");
            }

            card.setPrice(sellOrder.getPrice());
            putCard(card);

            log.info("OK !");
            return 0;

        } catch (Exception e){
            log.error("Received Exception : "+ e);
            return 1;
        }
    }

    @JmsListener(destination = "transaction.buy")
    @SendTo("notif.send")
    public int onBuyOrder(BuyOrderEntity buyOrder){
        try {
            log.info("Retrieved buyOrder :  "+buyOrder);
            if(!buyOrder.getAction().equals("BUY")){
                throw new TransactionError("bad_request");
            }

            CardDTO card = getCard(buyOrder.getCard_id());
            if (card.getPrice() == null || card.getUserId() == 0){
                throw new TransactionError("not_on_sale");
            }
            if (card.getUserId() == buyOrder.getUser_id()){
                throw new TransactionError("same_buyer_seller");
            }

            UserDTO buyer = getUserById(buyOrder.getUser_id());
            UserDTO seller = getUserById(card.getUserId());

            if(buyer.getBalance() < card.getPrice()){
                throw new TransactionError("no_funds");
            }
            buyer.setBalance(buyer.getBalance() - card.getPrice());
            seller.setBalance(seller.getBalance() + card.getPrice());
            putUser(buyer);
            putUser(seller);

            card.setUserId(buyer.getId());
            card.setPrice(0.0);
            putCard(card);

            TransactionDTO transaction = new TransactionDTO();
            transaction.setBuyerId(buyer.getId());
            transaction.setSellerId(seller.getId());
            transaction.setCardId(card.getId());

            transactionService.addTransaction(transaction);
            log.info("OK !");
            return 0;
        } catch (Exception e){
            log.error("Received Exception : "+ e);
            return 1;
        }
    }

    public UserDTO getUserById(int id) {
        return this.restTemplate.getForObject(this.UserEndpoint + "/user_id/" + id, UserDTO.class);
    }

    public CardDTO getCard(int id) {
        return this.restTemplate.getForObject(this.CardEndpoint + "/card/" + id, CardDTO.class);
    }

    public void modifyCard(int id, CardDTO cardDTO) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String resourceUrl = this.CardEndpoint + '/' + id;
        HttpEntity<CardDTO> requestUpdate = new HttpEntity<>(cardDTO, headers);
        this.restTemplate.exchange(resourceUrl, HttpMethod.PUT, requestUpdate, Void.class);
    }

    public CardDTO putCard(CardDTO cardDTO){
        this.restTemplate.put(CardEndpoint + "/card/" + cardDTO.getId(), cardDTO);
        return cardDTO;
    }
    public UserDTO putUser(UserDTO userDTO){
        this.restTemplate.put(UserEndpoint +"/user/" + userDTO.getLogin(), userDTO);
        return userDTO;
    }
    public boolean modifyUser(int id, UserDTO userDTO, double price) {
        if(userDTO.getBalance() > price) {
            userDTO.setBalance(userDTO.getBalance()-price);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            String resourceUrl = this.UserEndpoint + '/' + id;
            HttpEntity<UserDTO> requestUpdate = new HttpEntity<>(userDTO, headers);
            this.restTemplate.exchange(resourceUrl, HttpMethod.PUT, requestUpdate, Void.class);
            return true;
        } else {
            return false;
        }
    }

}
