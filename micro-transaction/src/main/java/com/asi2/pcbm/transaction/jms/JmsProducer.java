package com.asi2.pcbm.transaction.jms;

import com.asi2.pcbm.common.entities.BuyOrderEntity;
import com.asi2.pcbm.common.entities.SellOrderEntity;
import com.asi2.pcbm.transaction.config.JmsConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JmsProducer {

    @Autowired
    JmsConfig jmsConfig;

    public void update(BuyOrderEntity buyOrder){
        log.info("Attempting Send buy message to Topic: transaction.update");
        jmsConfig.jmsTemplate().convertAndSend("transaction.buy", buyOrder);
    }
    public void update(SellOrderEntity sellOrder){
        log.info("Attempting Send sell message to Topic: transaction.update");
        jmsConfig.jmsTemplate().convertAndSend("transaction.sell", sellOrder);
    }

}
