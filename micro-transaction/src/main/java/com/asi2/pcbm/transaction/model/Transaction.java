package com.asi2.pcbm.transaction.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "transactions")
@Getter
@Setter
@NoArgsConstructor
public class Transaction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transaction_gen")
	@SequenceGenerator(name= "transaction_gen", sequenceName = "transaction_seq", initialValue = 4)
	private Integer id;

	@DecimalMin("0.0")
	private BigDecimal price;
	private Integer  cardId;
	private Integer  buyerId;
	private Integer sellerId;

	private Date transactionDate;
}
