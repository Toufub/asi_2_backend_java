package com.asi2.pcbm.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class MicroUser {

	public static void main(String[] args) {
		SpringApplication.run(MicroUser.class, args);
	}

}
