package com.asi2.pcbm.user.controller;

import com.asi2.pcbm.common.exceptions.UserError;
import com.asi2.pcbm.user.jms.JmsProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.asi2.pcbm.user.model.User;
import com.asi2.pcbm.common.model.UserDTO;
import com.asi2.pcbm.user.tools.UserMapper;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.util.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    JmsProducer jmsProducer;

    /**
     * Read - Get one user
     * @param id The id of the user
     * @return A User object
     */
    @RequestMapping(method = RequestMethod.GET, value = "/api/user/{id}")
    public User getByUsername(@PathVariable String id) {
        try {
            return userService.getByUsername(id);
        } catch (UserError e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }
    /**
     * Read - Get one user
     * @param id The id of the user
     * @return A User object
     */
    @RequestMapping(method = RequestMethod.GET, value = "/api/user_id/{id}")
    public User getById(@PathVariable int id) {
        try {
            return userService.getUser(id);
        } catch (UserError e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    /**
     * Put - Update user
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/api/user/{id}")
    public User updateUser(@PathVariable String id, @RequestBody UserDTO userDTO){
        try {
            return userService.putUser(id, userDTO);
        } catch (UserError e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    /**
     * Delete - Delete an user
     * @param id - The id of the user to delete
     */
    @DeleteMapping("/api/users/{id}")
    public void delete(@PathVariable String id) {
        try {
            userService.deleteUser(id);
        } catch (UserError e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    /**
     * Read - Get all users
     * @return A list of User object fulfilled
     */
    @GetMapping("/api/users")
    public Iterable<UserDTO> getAll() {
        List<User> userList = (List<User>) userService.getAllUsers();
        List<UserDTO> userDTOList = new ArrayList<UserDTO>();
        for (User user : userList) {
            userDTOList.add(UserMapper.UserToDTO(user));
        }
        return userDTOList;
    }

    /**
     * Create - Add a new user
     * @param userDTO An object user
     * @return The user object saved
     */
    @PostMapping("/api/user")
    public UserDTO create(@RequestBody UserDTO userDTO) {
        this.jmsProducer.sendCreationMessage(userDTO);
        return userDTO;
        //return userService.createUser(userDTO);
    }

}
