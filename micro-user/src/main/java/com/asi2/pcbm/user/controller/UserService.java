package com.asi2.pcbm.user.controller;

import com.asi2.pcbm.common.model.UserDTO;
import com.asi2.pcbm.common.exceptions.UserError;
import com.asi2.pcbm.user.tools.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asi2.pcbm.user.model.User;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Iterable<User> getAllUsers(){
        return userRepository.findAll();
    }

    public User getUser(int id) throws UserError {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserError("not_found"));
    }

    public User getByUsername(String username) throws UserError {
        return userRepository.findByUsername(username)
        .orElseThrow(() -> new UserError("not_found"));
    }

    public void deleteUser(String username) throws UserError {
        User user = getByUsername(username);
        userRepository.delete(user);
    }

    public User createUser(UserDTO user) {
        return userRepository.save(UserMapper.DTOtoUser(user));
    }

    public User putUser(String id, UserDTO newUser) throws UserError {
        getByUsername(id);
        return userRepository.findByUsername(id)
                .map(user -> {
                    user.setBalance(newUser.getBalance());
                    return this.userRepository.save(user);
                })
                .orElseGet(() -> {
                    return null;
                });
    }
}
