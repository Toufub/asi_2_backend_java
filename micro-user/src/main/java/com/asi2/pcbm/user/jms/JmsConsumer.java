package com.asi2.pcbm.user.jms;

import com.asi2.pcbm.user.config.JmsConfig;
import com.asi2.pcbm.user.controller.UserService;
import com.asi2.pcbm.common.model.UserDTO;
import com.asi2.pcbm.user.model.User;
import com.asi2.pcbm.user.tools.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JmsConsumer {

    @Autowired
    private final UserService userService;

    @Autowired
    JmsConfig jmsConfig;

    public JmsConsumer(UserService userService) {
        this.userService = userService;
    }

    @JmsListener(destination = "user.create")
    @SendTo("card.add_to_new_user")
    public UserDTO onCreateMessage(UserDTO user) {
        try{
            log.info("Received Message: "+ user.getLogin());
            User created_user = userService.createUser(user);
            return UserMapper.UserToDTO(created_user);
        } catch(Exception e) {
            log.error("Received Exception : "+ e);
            throw e;
        }
    }

}
