package com.asi2.pcbm.user.jms;

import com.asi2.pcbm.common.model.UserDTO;
import com.asi2.pcbm.user.config.JmsConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JmsProducer {

    @Autowired
    JmsConfig jmsConfig;

    public void sendCreationMessage(UserDTO message){
        try{
            log.info("Attempting Send message to Topic: user.create");
            jmsConfig.jmsTemplate().convertAndSend("user.create", message);
        } catch(Exception e){
            log.error("Recieved Exception during send Message: ", e);
        }
    }

}
