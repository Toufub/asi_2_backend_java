package com.asi2.pcbm.user.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_gen")
	@SequenceGenerator(name= "user_gen", sequenceName = "user_seq", initialValue = 4)
	private Integer id;

	@Column(unique = true)
	private String login;
	private String lastName;
	private String surName;
	private String pwd;
	private double balance;

	public User(String login, String lastName, String surName, String pwd, double account) {
		this.login = login;
		this.lastName = lastName;
		this.surName = surName;
		this.pwd = pwd;
		this.balance = account;
	}
	@Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof User)) return false;
        User other = (User) o;
        return Objects.equals(this.getId(), other.getId());
    }
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
