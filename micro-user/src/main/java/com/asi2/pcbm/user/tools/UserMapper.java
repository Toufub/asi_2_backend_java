package com.asi2.pcbm.user.tools;

import com.asi2.pcbm.common.model.UserDTO;

import com.asi2.pcbm.user.model.User;

public class UserMapper {
	public static UserDTO UserToDTO(User a) {
		return new UserDTO(a.getId(), a.getLogin(), a.getLastName(), a.getSurName(), a.getPwd(), a.getBalance());
	}

	public static User DTOtoUser(UserDTO aDTO) {
		return new User(aDTO.getLogin(), aDTO.getLastName(), aDTO.getSurName(), aDTO.getPwd(), aDTO.getBalance());
	}
}

